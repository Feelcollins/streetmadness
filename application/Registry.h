//
//  Registry.h
//  BaconBoxApp
//
//  Created by Philippe Dubé-Tremblay on 12-06-02.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#ifndef SM_REGISTRY_H
#define SM_REGISTRY_H

#include <BaconBox.h>

namespace StreetMadness {
	class Registry{
    public:
        static BaconBox::State* playState;
        static std::string playerName;
        static bool controlHorizontal;
        static int score;
        static std::multimap<int, std::string, std::greater<int> > topTen;
	};
}


#endif
