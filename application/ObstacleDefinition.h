//
//  ObstacleDefinition.h
//  BaconBoxApp
//
//  Created by PHILIPPE DUBE-TREMBLAY on 2012-09-18.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#ifndef __BaconBoxApp__ObstacleDefinition__
#define __BaconBoxApp__ObstacleDefinition__

#include <BaconBox.h>

namespace StreetMadness {
    typedef enum ObstacleType{
        MOB = 0,
        SLOWER = 1,
        BONUS = 2,
        DECOR = 3
    } ObstacleType;
	class ObstacleDefinition{
    public:
        
        ObstacleDefinition();
        ~ObstacleDefinition(){}
        
        const std::string& getName() const { return name; }
        void setName(std::string newName) { name = newName; }
        const std::list<std::string> getExplosionNames() const { return explosionNames; }
        void addExplosion(const std::string& newExplosionName) { explosionNames.push_back(newExplosionName); }
        const std::string& getExplosionSoundName() const {return explosionSoundName; }
        void setExplosionSoundName(std::string newName) { explosionSoundName = newName; }
        const std::string& getScratchSoundName() const {return scratchSoundName; }
        void setScratchSoundName(std::string newName) { scratchSoundName = newName; }
        const ObstacleType getType() const {return obsType; }
        void setType(ObstacleType newType){ obsType = newType; }
        float getZ() const {return z; }
        void setZ(float newZ) { z = newZ; }
        float getAngle() const {return angle; }
        void setAngle(float newAngle) { angle = newAngle; }
        float getQuakeForce() const {return quakeForce; }
        void setQuakeForce(float newQuakeForce){ quakeForce = newQuakeForce; }
        float getQuakeDecay() const { return quakeDecay; }
        void setQuakeDecay(float newQuakeDecay){ quakeDecay = newQuakeDecay; }
        float getYSpeedFactor() const {return ySpeedFactor; }
        void setYSpeedFactor(float newYSpeedFactor){ ySpeedFactor = newYSpeedFactor; }
        float getXDelta() const { return xDelta; }
        void setXDelta(float newXDelta) { xDelta = newXDelta; }
        float getHealthModifier() const { return healthModifier; }
        void setHealthModifier(float newHealthModifier) { healthModifier = newHealthModifier; }
        float getSpeedModifier() const { return speedModifier; }
        void setSpeedModifier(float newSpeedModifier){ speedModifier = newSpeedModifier; }
        int getScoreBonus() const { return scoreBonus; }
        void setScoreBonus(int newScoreBonus){ scoreBonus = newScoreBonus; }
        float getCollisionRadius() const { return collisionRadius; }
        void setCollisionRadius(float newCollisionRadius){ collisionRadius = newCollisionRadius; }
        float getScratchRadius() const { return scratchRadius; }
        void setScratchRadius(float newScratchRadius){ scratchRadius = newScratchRadius; }
        
        const std::string& getAnimationName() const { return animationName; }
        void setAnimationName(std::string newName) { animationName = newName; }
        int getFps() const { return fps; }
        void setFps(int newFps) { fps = newFps; }
        const std::vector<unsigned int> getFrames() const { return frames; }
        void setFrames(std::vector<unsigned int> newFrames) { frames = newFrames; }
        const BaconBox::Vector2 getFrameSize() const { return frameSize; }
        void setFrameSize(BaconBox::Vector2 newFrameSize) { frameSize = newFrameSize; }
        
        
        int getAmmos() const { return ammos; }
        void setAmmos(int newAmmos){ ammos = newAmmos; }
        float getShieldPower() const { return shieldPower; }
        void setShieldPower(float newShieldPower){ shieldPower = newShieldPower; }
        
    private:
        
        void init();
        std::string name;
        std::list<std::string> explosionNames;
		std::string explosionSoundName;
		std::string scratchSoundName;
        ObstacleType obsType;
        float z;
		float angle;
		float quakeForce;
		float quakeDecay;
        float ySpeedFactor;
        float xDelta;
		float healthModifier;
		float speedModifier;
        int scoreBonus;
        float collisionRadius;
        float scratchRadius;
        int ammos;
        float shieldPower;
        
        std::string animationName;
        int fps;
        std::vector<unsigned int> frames;
        BaconBox::Vector2 frameSize;
        
	};
}

#endif /* defined(__BaconBoxApp__ObstacleDefinition__) */
