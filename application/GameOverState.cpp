//
//  GameOverState.cpp
//  BaconBoxApp
//
//  Created by PHILIPPE DUBE-TREMBLAY on 2012-11-08.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#include <iostream.h>
#include <fstream.h>
#include "GameOverState.h"
#include "Registry.h"

using namespace BaconBox;

namespace StreetMadness {
    GameOverState::GameOverState(const std::string &newName) : State(newName), highScoreMade(false){
        
        ResourceManager::loadTextureRelativePath("gameOverBG", "GameOver.png");
        ResourceManager::loadTextureRelativePath("btnMenu", "Bouton-Menu.png");
        ResourceManager::loadTextureRelativePath("btnRetry", "Bouton-Retry.png");
        
        ResourceManager::loadFontRelativePath("targa", "Targa.ttf");
        
        InputManager::getDefaultKeyboard()->connectKeyRelease(this, &GameOverState::onKeyPressed);
        InputManager::getDefaultPointer()->connectButtonRelease(this, &GameOverState::onClick);
        
        _bg = new Sprite("gameOverBG");
        _btnMenu = new Sprite("btnMenu");
        _btnRetry = new Sprite("btnRetry");
        add(_bg);
        add(_btnMenu);
        add(_btnRetry);
        _playerNameLabel = new Text("targa");
        _playerScoreLabel = new Text("targa");
        add(_playerNameLabel);
        add(_playerScoreLabel);
    }
    void GameOverState::onGetFocus(){
        init();
        if (Registry::topTen.rbegin()->first < Registry::score) {
            highScoreMade = true;
            std::stringstream ss;
            ss << Registry::score;
            _playerScoreLabel->setText("New Highscore : " + ss.str());
        }else{
            highScoreMade = false;
        }
    }
    void GameOverState::init(){
        
        _bg->setAngle(90);
        _bg->setScaling(2,2);
        _bg->setPosition(camera.getPosition());
        
        _btnMenu->setAngle(90);
        _btnRetry->setAngle(90);
        _btnMenu->setScaling(Vector2(1.5,1.5));
        _btnRetry->setScaling(Vector2(1.5,1.5));
        _btnMenu->setPosition(Vector2(500,0.5/4.0*camera.getHeight()-_btnMenu->getHeight()*0.5) + camera.getPosition());
        _btnRetry->setPosition(Vector2(500,3.5/4.0*camera.getHeight()-_btnRetry->getHeight()*0.5) + camera.getPosition());
        
        _playerNameLabel->setText("");
        _playerNameLabel->setPosition(Vector2(camera.getWidth()*0.5, camera.getHeight()*0.75));
        _playerNameLabel->setColor(Color::WHITE);
        _playerNameLabel->setAngle(90);
        _playerName.str("");
        _playerName << Registry::playerName;
        
        _playerScoreLabel->setPosition(Vector2(camera.getWidth()*0.25, camera.getHeight()*0.75));
        _playerScoreLabel->setColor(Color::WHITE);
        _playerScoreLabel->setAngle(90);
        
    }
    void GameOverState::update(){
        if(highScoreMade){
            _playerNameLabel->setText("Enter your name : " + _playerName.str());
        }else{
            _playerNameLabel->setText("You didn't made the top ten try again.");
        }
    }
    void GameOverState::onKeyPressed(KeySignalData data){
        
        if (data.keyChar > 'a' && data.keyChar < 'z'){
            _playerName << data.keyChar;
        }else if (data.key == Key::BACKSPACE){
            if (_playerName.str().length() > 0) {
                std::string temp = _playerName.str();
                temp.erase(temp.end()-1);
                _playerName.str("");
                _playerName << temp;
                
            }
        }
    }
    void GameOverState::onClick(PointerButtonSignalData data){
        Registry::playerName = _playerName.str();
        if (_btnRetry->getAxisAlignedBoundingBox().overlaps(camera.screenToWorld(data.getPosition()))) {
            if (highScoreMade && Registry::playerName != "") {
                
                saveScoreToRegistry();
                saveScoreToFile();
            }
            Engine::playState("PlayState");
            
        }else if (_btnMenu->getAxisAlignedBoundingBox().overlaps(camera.screenToWorld(data.getPosition()))) {
            if (highScoreMade && Registry::playerName != "") {
                
                saveScoreToRegistry();
                saveScoreToFile();
            }
            Engine::playState("MenuState");
        }
    }
    void GameOverState::saveScoreToRegistry(){
        std::multimap<int, std::string>::iterator it = --Registry::topTen.end();
        Registry::topTen.erase(it);
        Registry::topTen.insert(std::pair<int, std::string>(Registry::score,Registry::playerName));
    }
    void GameOverState::saveScoreToFile(){
        
        std::string path = ResourcePathHandler::getResourcePathFor("Scores.txt");
        std::stringstream aStream;
        std::ofstream myFile;
        myFile.open(path.c_str());
        
        std::multimap<int, std::string>::iterator it;
        for(it = Registry::topTen.begin(); it != Registry::topTen.end();it++){
            // écrire une ligne.
            
            myFile << it->second << "\t" << it->first << std::endl;
        }
        myFile.close();
    }
    
}