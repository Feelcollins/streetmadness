//
//  Obstacle.cpp
//  BaconBoxApp
//
//  Created by PHILIPPE DUBE-TREMBLAY on 2012-09-06.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#include "Obstacle.h"
#include "PlayState.h"

using namespace BaconBox;

namespace StreetMadness {
    const char* Obstacle::HEALTH_INDICATOR_PATH = "FlecheVerte.png";
    const char* Obstacle::DANGER_INDICATOR_PATH = "FlecheRouge.png";
    const char* Obstacle::OBSTACLE_INDICATOR_PATH = "FlecheOrange.png";
    const char* Obstacle::SHIELD_INDICATOR_PATH = "FlecheTurquoise.png";
    const char* Obstacle::ROCKET_INDICATOR_PATH = "FlecheBlanche.png";
    
    Obstacle::Obstacle(const ObstacleDefinition& def, std::list<const ExplosionDefinition*> explosionsDef) : Sprite(def.getName(),Vector2(),def.getFrameSize()), _isDestroyed(false), _veloIsApplyToParticles(false), marker(0) {
        _definition = &def;
        float xVelo = Random::getRandomFloat(-_definition->getXDelta(), _definition->getXDelta());
        velocityRatios = Vector2(xVelo,_definition->getYSpeedFactor());
        
        //animate the obstacle.
        if (def.getAnimationName() != "") {
            addAnimation(def.getAnimationName(),  AnimationDefinition(def.getFrames(),1.0/def.getFps()));
            startAnimation(def.getAnimationName());
        }
        std::string path = "";
        switch (def.getType()) {
            case MOB:
                path = DANGER_INDICATOR_PATH;
                break;
            case SLOWER:
                path = OBSTACLE_INDICATOR_PATH;
                break;
            case BONUS:
                if (def.getShieldPower() > 0) {
                    path = SHIELD_INDICATOR_PATH;
                }else if (def.getHealthModifier() > 0)
                {
                    path = HEALTH_INDICATOR_PATH;
                }else{
                    path = ROCKET_INDICATOR_PATH;
                }
                break;
        }
        if (!path.empty()) {
            marker = new Sprite(path);
        }
        
        //create the sprite emiter for the destruction.
        if (def.getExplosionNames().size() > 0) {
            //get build the sprite emiter with the definitions
            while (explosionsDef.size() != 0) {
                
                const ExplosionDefinition* currentDefinition = explosionsDef.front();
                
                GraphicElement<Collidable>* tempExplosion = new GraphicElement<Collidable>(currentDefinition->getName()
                                                                                           , Vector2()
                                                                                           , Vector2(currentDefinition->getFrameWidth(), currentDefinition->getFrameHeight()));

                if (currentDefinition->getAnim() != 0) {
                    tempExplosion->addAnimation("mainAnim",  *(currentDefinition->getAnim()));
                    tempExplosion->startAnimation("mainAnim");
                }

                SpriteEmitter* explosion = new SpriteEmitter(tempExplosion);
                explosion->setNbParticlesToShoot(currentDefinition->getNbEmissions());
                explosion->setMaximumNbParticles(currentDefinition->getNbEmissions());
                explosion->setShootingAngle(currentDefinition->getAngle());
                explosion->setShootingAngleVariance(currentDefinition->getAngleRange());
                explosion->setShootingForce(currentDefinition->getForce());
                explosion->setShootingForceVariance(currentDefinition->getForceRange());
                explosion->setTimeBetweenSpawns(0.00001);
                explosion->setLifeSpan(-1);
                
                ParticlePhase aPhase = ParticlePhase();
                aPhase.phaseDuration = 1;
                explosion->getPhases().push_back(aPhase);

                _explosions.push_back(explosion);
                explosionsDef.pop_front();
            }
        }
        
        rotate(180);
    }
    
    Obstacle::~Obstacle(){
        std::list<SpriteEmitter*>::iterator it = _explosions.begin();
        while (it != _explosions.end()) {
            delete (*it);
            it++;
        }
    }
    void Obstacle::destroy(){
        
        _isDestroyed = true;
        Registry::playState->camera.shake(_definition->getQuakeForce(),1.0);
        //play sound
        MusicEngine::playSoundFX(_definition->getExplosionSoundName());
        std::list<SpriteEmitter*>::iterator it = _explosions.begin();
        while (it != _explosions.end()) {
            (*it)->setToDeleteWhenDone(true);
            (*it)->start();
            
            it++;
        }
    }
    
    void Obstacle::update(){
        
        Sprite::update();
        if (marker) {
            marker->setPosition(Vector2(getPosition().x,0));
        }
        
        std::list<SpriteEmitter*>::iterator it = _explosions.begin();
        while (it != _explosions.end()) {
            
            (*it)->update();
            
            it++;
        }
        
        if (_isDestroyed && !_veloIsApplyToParticles) {
            ApplyVelocityToParticles();
        }
    }
    
    void Obstacle::render(){
        if(_isDestroyed){
        std::list<SpriteEmitter*>::iterator it = --_explosions.end();
            while (it != _explosions.end()) {
                Particle<GraphicElement<Collidable, NonManageable> > test = ((*it)->getParticles().front().second);
                
                (*it)->setPosition(getPositionCenter() - (*it)->getDefaultGraphic()->getSize()/2);
                
                (*it)->render();
                it--;
            }
        }
        else{
            if (marker && getPosition().y + getHeight() < 0) {
                marker->render();
            }else{
                Sprite::render();
            }
        }
    }
    
    void Obstacle::ApplyVelocityToParticles(){
        std::list<SpriteEmitter*>::iterator it = _explosions.begin();
        while (it != _explosions.end()) {
            for (int i = 0; i < (*it)->getParticles().size() ; ++i) {
            
                if ((*it)->getParticles()[i].second.graphic) {
                    (*it)->getParticles()[i].second.graphic->setVelocity((*it)->getParticles()[i].second.graphic->getVelocity() + Vector2(0,getYVelocity()));
                }
            }
            it++;
        }
        _veloIsApplyToParticles = true;
    }
}