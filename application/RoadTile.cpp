//
//  Tile.cpp
//  BaconBoxApp
//
//  Created by Philippe Dubé-Tremblay on 12-05-13.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#include "RoadTile.h"

using namespace BaconBox;

namespace StreetMadness {
    RoadTile::RoadTile(){
    
    }
    
    RoadTile::RoadTile(std::string& path, TileInfo newInfo)
    : Sprite(path)
    , _infos(newInfo)
    {
    }
    
    void RoadTile::setTileInfo(TileInfo& newInfo){
        _infos = newInfo;
    }
    void RoadTile::move(float xDelta, float yDelta){
        std::list<Sprite*>::iterator it;
        for (it = _treeList.begin(); it != _treeList.end(); it++) {
            (*it)->move(xDelta,yDelta);
        }
        Sprite::move(xDelta, yDelta);
    }
    TileInfo& RoadTile::getTileInfo(){
        return _infos;
    }
    
    void RoadTile::addTree(BaconBox::Sprite *newTree, Vector2 pos){
        _treeList.push_back(newTree);
        _treeList.back()->setPosition(pos);
    }
    
    int RoadTile::clearTrees(){
        int i = _treeList.size();
        while (_treeList.size()) {
            _treeList.pop_front();
        }
        return i;
    }
    
    void RoadTile::renderTrees(){
        std::list<Sprite*>::iterator it;
        for (it = _treeList.begin(); it != _treeList.end(); it++) {
            (*it)->render();
        }
    }
}