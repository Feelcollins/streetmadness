//
//  Header.h
//  BaconBoxApp
//
//  Created by Philippe Dubé-Tremblay on 12-05-10.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#ifndef SM_ROADMANAGER_h
#define SM_ROADMANAGER_h

#include <BaconBox.h>
#include "TileBag.h"
#include "Registry.h"
#include "ObstacleManager.h"

namespace StreetMadness {
	class RoadManager{
        static const int BUFFER_SIZE = 5;
        static const int POURCENT_CHANCE_TREES = 25;
        static const int POURCENT_CHANCE_OBSTACLES = 50;
        static const int RANDOM_TREE_XY = 30;
        
        static const int POURCENT_CHANCE_MOB = 40;
        static const int POURCENT_CHANCE_SLOWER = 20;
        static const int POURCENT_CHANCE_BONUS = 5;
        static const int POURCENT_CHANCE_DECOR = 35;
        
    public:
        RoadManager();
        ~RoadManager();
        void init();
        void update(float speed);
        void renderRoad();
        void renderTrees();
        void renderObstacles();
        void verifyCollision();
        const TileInfo* getTileInfo(float yPosition) const;
    private:
        void initTiles();
        void initGrass();
        void moveTiles(float speed);
        void moveGrass(float speed);
        void updateTiles();
        void loopGrass();
        void setPropsOnTile();
        std::list<RoadTile*> _tileList;
        std::list<BaconBox::Sprite*> _grassList;
        std::list<BaconBox::Sprite*> _usedTrees;
        ObstacleManager _obstacleManager;
        TileBag _tileBag;
	};
}

#endif
