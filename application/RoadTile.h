//
//  Tile.h
//  BaconBoxApp
//
//  Created by Philippe Dubé-Tremblay on 12-05-13.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//


#ifndef SM_ROADTILE_h
#define SM_ROADTILE_h

#include <BaconBox.h>
#include "Obstacle.h"

namespace StreetMadness {
	struct TileInfo{
        float rotationAngle;
        float leftLimit;
        float rightLimit;
        float leftEnnemiLimit;
        float rightEnnemiLimit;
        float offset;
        std::vector<BaconBox::Vector2> treesPositions;
        float treeRadius;
        std::pair<float, float> medians;
        std::vector<int> childs;
        
    };
    class RoadTile : public BaconBox::Sprite{
    public:
        RoadTile();
        RoadTile(std::string& path, TileInfo newInfo);
        TileInfo& getTileInfo();
        virtual void move(float xDelta, float yDelta);
        void setTileInfo(TileInfo& newInfo);
        void addTree(BaconBox::Sprite* newTree,BaconBox::Vector2 pos);
        int clearTrees();
        void renderTrees();
    private:
        TileInfo _infos;
        std::list<BaconBox::Sprite*> _treeList;
        
    };
}

#endif
