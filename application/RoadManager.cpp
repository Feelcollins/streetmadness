//
//  RoadManager.cpp
//  BaconBoxApp
//
//  Created by Philippe Dubé-Tremblay on 12-05-10.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#include "RoadManager.h"

using namespace BaconBox;

namespace StreetMadness {
    
    RoadManager::RoadManager(){
        ResourceManager::loadTextureRelativePath("grass", "grass.png");
                
        for (int i = 0; i < 11; ++i) {
            _grassList.push_back(new Sprite("grass"));
        }
        init();
    }
    RoadManager::~RoadManager(){
        
        while(_grassList.size()){
            delete _grassList.back();
            _grassList.erase((--_grassList.end()));
        }
    }
    void RoadManager::init(){
        initTiles();
        initGrass();
        _obstacleManager.init();
    }
    
    void RoadManager::update(float speed){
        moveTiles(speed);
        updateTiles();
        moveGrass(speed);
        loopGrass();
        _obstacleManager.update(speed);
    }
    
    void RoadManager::renderRoad(){
        
        std::list<Sprite*>::iterator it2;
        for (it2 = _grassList.begin(); it2 != _grassList.end(); it2++) {
            (*it2)->render();
        }

        std::list<RoadTile*>::iterator it;
        for (it = _tileList.begin(); it != _tileList.end(); it++) {
            (*it)->render();
        }
    }
    void RoadManager::renderTrees(){
        
        std::list<RoadTile*>::iterator it;
        for (it = _tileList.begin(); it != _tileList.end(); it++) {
            (*it)->renderTrees();
        }
    }
    void RoadManager::renderObstacles(){
        _obstacleManager.render();
    }
    
    void RoadManager::verifyCollision() {
        _obstacleManager.resolveCollision();
    }
    
    const TileInfo* RoadManager::getTileInfo(float yPosition) const {
        std::list<RoadTile*>::const_iterator it;
        for (it = _tileList.begin(); it != _tileList.end(); it++) {
            
            if ((*it)->getYPosition() < yPosition) {
                return &((*it)->getTileInfo());
            }
        }
        return 0;
    }
    
    //////////////////////////////////////////////
    // TILE HANDLING
    //////////////////////////////////////////////
    void RoadManager::initTiles(){
        std::list<RoadTile*>::iterator it;
        for ( it = _tileList.begin(); it != _tileList.end(); it++) {
            (*it)->clearTrees();
        }
        _tileList.clear();
        _usedTrees.clear();
        _tileList.push_back( _tileBag.getSpecificRoadTile(_tileList, 0) );
        _tileList.back()->setPosition(_tileList.back()->getTileInfo().offset, 500);
        
        for (int i=1; i<BUFFER_SIZE; ++i) {
            float yPos = _tileList.back()->getYPosition();
            _tileList.push_back( _tileBag.getSpecificRoadTile(_tileList, 0));
            _tileList.back()->setPosition(Vector2(_tileList.back()->getTileInfo().offset, yPos - _tileList.back()->getHeight()+1));
        }
    }
    
    void RoadManager::moveTiles(float speed){
        std::list<RoadTile*>::iterator it;
        for (it = _tileList.begin(); it != _tileList.end(); it++) {
            
            (*it)->moveY(speed);
        }
    }
    
    void RoadManager::updateTiles(){
        if (_tileList.front()->getYPosition() > Registry::playState->getCamera().getHeight() + RANDOM_TREE_XY) {
            int i = _tileList.front()->clearTrees();
            
            while (i > 0) {
                _usedTrees.pop_front();
                --i;
            }
            _tileList.pop_front();
            
            float yPos = _tileList.back()->getYPosition();
            
            _tileList.push_back( _tileBag.getRoadTile(_tileList));
            RoadTile* current = _tileList.back();
            current->setPosition(Vector2(current->getTileInfo().offset, yPos - current->getHeight()+1));
            setPropsOnTile();
        }
    }
    
    void RoadManager::setPropsOnTile()
    {
        RoadTile* current = _tileList.back();
        
        TileInfo infos = current->getTileInfo();
        for (int i=0; i<infos.treesPositions.size(); ++i) {
            int rand1 = Random::getRandomInteger(-RANDOM_TREE_XY, RANDOM_TREE_XY);
            if (Random::getRandomInteger(0, 100) < POURCENT_CHANCE_TREES) {
                Sprite* prop = _tileBag.getProp(_usedTrees);
                if (prop) {
                    _usedTrees.push_back(prop);
                    Vector2 position = Vector2(0,current->getYPosition()) + infos.treesPositions[i];
                    Vector2 offset(prop->getSize()/2);
                    Vector2 randomFactor(rand1,Random::getRandomInteger(-RANDOM_TREE_XY, RANDOM_TREE_XY));
                    
                    current->addTree(prop,position - offset + randomFactor);
                }
            }
        }
        //TODO : teaking HERE!!!
        if (Random::getRandomInteger(0, 100) <= POURCENT_CHANCE_OBSTACLES) {
        
            int obsType = Random::getRandomInteger(0, POURCENT_CHANCE_BONUS+POURCENT_CHANCE_DECOR+POURCENT_CHANCE_MOB+POURCENT_CHANCE_SLOWER);
        
            if (obsType <= POURCENT_CHANCE_MOB) {
                obsType = 0;
            }else if (obsType <= POURCENT_CHANCE_MOB+POURCENT_CHANCE_SLOWER){
                obsType = 1;
            }else if (obsType <= POURCENT_CHANCE_MOB+POURCENT_CHANCE_SLOWER+POURCENT_CHANCE_DECOR){
                obsType = 3;
            }else{
                obsType = 2;
            }
    
            Obstacle* obs = _obstacleManager.addObstacle((ObstacleType)obsType);
            obs->setYPosition(current->getYPosition());
            obs->setXPosition(Random::getRandomFloat(infos.leftEnnemiLimit, infos.rightEnnemiLimit-obs->getWidth()) + Registry::playState->getCamera().getWidth()/2);
            
        }
    }
    
    //////////////////////////////////////////////
    // GRASS HANDLING
    //////////////////////////////////////////////
    void RoadManager::initGrass(){
        std::list<BaconBox::Sprite*>::iterator it;
        int i = 0;
        for (it = _grassList.begin(); it != _grassList.end(); it++) {
            (*it)->setYPosition(500-i*((*it)->getHeight()-1));
            ++i;
        }
    }
    
    
    void RoadManager::moveGrass(float speed){
        std::list<Sprite*>::iterator it;
        for (it = _grassList.begin(); it != _grassList.end(); it++) {
            (*it)->moveY(speed);
        }
    }
    
    void RoadManager::loopGrass(){
        if (_grassList.front()->getYPosition() > Registry::playState->getCamera().getHeight()) {
            float yPos = _grassList.back()->getYPosition();
            _grassList.push_back(_grassList.front());
            _grassList.pop_front();
            
            _grassList.back()->setYPosition(yPos-_grassList.back()->getHeight()+1);
        }
    }
}