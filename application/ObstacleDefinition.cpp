//
//  ObstacleDefinition.cpp
//  BaconBoxApp
//
//  Created by PHILIPPE DUBE-TREMBLAY on 2012-09-18.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#include "ObstacleDefinition.h"

using namespace BaconBox;

namespace StreetMadness {
    ObstacleDefinition::ObstacleDefinition()
    : name("")
    , explosionSoundName("")
    , scratchSoundName("")
    , obsType(MOB)
    , z(0)
    , angle(0)
    , quakeForce(0)
    , quakeDecay(0)
    , ySpeedFactor(0)
    , xDelta(0)
    , healthModifier(0)
    , speedModifier(0)
    , scoreBonus(0)
    , animationName("")
    , fps(0)
    , frameSize(Vector2())
    {
        
    }
}