//
//  TileBag.h
//  BaconBoxApp
//
//  Created by Philippe Dubé-Tremblay on 12-05-13.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#ifndef SM_TILEBAG_h
#define SM_TILEBAG_h

#include <BaconBox.h>
#include "RoadTile.h"

namespace StreetMadness {
	class TileBag{
        static const int TILECOPY = 4;
        static const int TREE_BUFFER_SIZE = 9;
    public:
        TileBag();
        ~TileBag();
        std::vector<std::vector<RoadTile*> > bag;
        std::vector<BaconBox::Sprite*> propBag;
        BaconBox::Sprite* getProp(std::list<BaconBox::Sprite*> currentTrees);
        RoadTile* getSpecificRoadTile(std::list<RoadTile*> currentTiles, int index);
        RoadTile* getRoadTile(std::list<RoadTile*> currentTiles);
    private:
        void load();
    };
}

#endif
