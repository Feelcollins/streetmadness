#include <BaconBox.h>
#include "PlayState.h"
#include "MenuState.h"
#include "GameOverState.h"

int main(int argc, char* argv[]) {
	// Initialize BaconBox
	BaconBox::Engine::application(argc, argv);

	// We set the number of updates per second to be executed.
	BaconBox::Engine::setUpdatesPerSecond(60);

	// We set the minimum number of frames per second that are
	// tolerated before sacrificing on the number of updates per second.
	BaconBox::Engine::setMinFps(5);

	// We initialize the engine with a screen resolution.
	BaconBox::Engine::initializeEngine(640, 960);

	// We add our states to the engine. The first state added
	// automatically starts playing.
	BaconBox::Engine::addState(new StreetMadness::MenuState());
    BaconBox::Engine::playState("MenuState");
	BaconBox::Engine::addState(new StreetMadness::PlayState());
	BaconBox::Engine::addState(new StreetMadness::GameOverState());
    
	// Then start everything.
	BaconBox::Engine::showMainWindow();

	return 0;
}
