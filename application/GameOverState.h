//
//  MenuState.h
//  BaconBoxApp
//
//  Created by PHILIPPE DUBE-TREMBLAY on 2012-10-30.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#ifndef SM_GAMEOVERSTATE_H
#define SM_GAMEOVERSTATE_H

#include <BaconBox.h>

namespace StreetMadness {

    class GameOverState : public BaconBox::State {
    public:
        GameOverState(const std::string &newName = std::string("GameOverState"));
        void init();
        void update();
        void onGetFocus();
    private:
        void onKeyPressed(BaconBox::KeySignalData data);
        void onClick(BaconBox::PointerButtonSignalData data);
        void saveScoreToFile();
        void saveScoreToRegistry();
        
        bool highScoreMade;
        BaconBox::Sprite* _btnMenu;
        BaconBox::Sprite* _btnRetry;
        BaconBox::Sprite* _bg;
        std::stringstream _playerName;
        BaconBox::Text* _playerScoreLabel;
        BaconBox::Text* _playerNameLabel;
        
	};
}

#endif
