#ifndef SM_PLAYSTATE_H
#define SM_PLAYSTATE_H

#include <BaconBox.h>
#include "Player.h"
#include "RoadManager.h"
#include "HudManager.h"

namespace StreetMadness {
    
    const float COLLISION_LIMIT = 0.3;
    const float SCRATCH_LIMIT = 0.43;
    const float BONUS_LIMIT = 0.5;
    const float REBOUND = 0.15;
    const int PIXEL_PER_METER = 24;
    
	class PlayState : public BaconBox::State {
    public:
        PlayState(const std::string &newName = std::string("PlayState"));
        ~PlayState();
        void init();
        void onGetFocus();
        void update();
        void render();
        const unsigned int getCurrentScore() { return _currentScore; } const
        void addPoints (unsigned int points) { _currentScore += points; }
        Player* getPlayer() { return _player; }
        void explosion();
        const TileInfo* getCurrentTileInfo(const BaconBox::Sprite* spriteOnTile) const;
    private:
        void onKeyHold(BaconBox::KeySignalData);
        void onKeyPress(BaconBox::KeySignalData);
        void onKeyRelease(BaconBox::KeySignalData);
        void onClick(BaconBox::PointerButtonSignalData);
        void easeTurning();
        bool playerOnMargin();
        
        RoadManager* _roadManager;
        HudManager* _hudManager;
        Player * _player;
        BaconBox::Sprite* _pause;
        BaconBox::Sprite* _pauseScreen;
        bool _paused;
        float _currentScore;
	};
}

#endif // SM_PLAYSTATE_H
