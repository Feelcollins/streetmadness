//
//  EnnemiManager.cpp
//  BaconBoxApp
//
//  Created by PHILIPPE DUBE-TREMBLAY on 2012-09-06.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#include "ObstacleManager.h"
#include "PlayState.h"

using namespace BaconBox;

namespace StreetMadness {
    
    ObstacleManager::ObstacleManager(){
        
        ResourceManager::loadTextureRelativePath(Obstacle::ROCKET_INDICATOR_PATH, Obstacle::ROCKET_INDICATOR_PATH);
        ResourceManager::loadTextureRelativePath(Obstacle::SHIELD_INDICATOR_PATH, Obstacle::SHIELD_INDICATOR_PATH);
        ResourceManager::loadTextureRelativePath(Obstacle::HEALTH_INDICATOR_PATH, Obstacle::HEALTH_INDICATOR_PATH);
        ResourceManager::loadTextureRelativePath(Obstacle::OBSTACLE_INDICATOR_PATH, Obstacle::OBSTACLE_INDICATOR_PATH);
        ResourceManager::loadTextureRelativePath(Obstacle::DANGER_INDICATOR_PATH, Obstacle::DANGER_INDICATOR_PATH);
        
        
        ResourceManager::loadSoundRelativePath("scratch", "scratch.wav");
        loadData();
    }
    ObstacleManager::~ObstacleManager(){
        //Destroy the list.
        std::list<Obstacle*>::iterator it;
        for (it = obstacleList.begin(); it != obstacleList.end(); it++) {
            
            delete (*it);
        }
    }
    void ObstacleManager::init(){
        obstacleList.clear();
    }
    void ObstacleManager::loadData(){
        
        std::string path = ResourcePathHandler::getResourcePathFor("levelDescription.json");
        
        JsonSerializer serializer;
        BaconBox::Value reader;
        serializer.readFromFile(path, reader);
        Object myObject = reader.getObject();
        Array soundSet = (myObject.find("soundEffects"))->second.getArray();
        
        //load all obstacles
        for (int i=0; i<soundSet.size(); ++i) {
            
            Object anObstacle = soundSet[i].getObject();
            
            std::string name = anObstacle.find("name")->second.getString();
            
            std::string path = anObstacle.find("path")->second.getString();
            
            ResourceManager::loadSoundRelativePath(name, path);
            
        }
        
        Array obstacleSet = (myObject.find("obstacles"))->second.getArray();
        //load all obstacles
        for (int i=0; i<obstacleSet.size(); ++i) {
            
            Object anObstacle = obstacleSet[i].getObject();
            
            ObstacleDefinition def;
            
            std::string name = anObstacle.find("name")->second.getString();
            
            ResourceManager::loadTextureRelativePath(name, anObstacle.find("image")->second.getString());
            def.setName(name);
            def.setHealthModifier(anObstacle.find("health")->second.getFloat());
            def.setSpeedModifier(-anObstacle.find("speedDamage")->second.getFloat());
            def.setCollisionRadius(anObstacle.find("frameWidth")->second.getFloat());
            def.setQuakeForce(anObstacle.find("quakeForce")->second.getFloat());
            def.setQuakeDecay(anObstacle.find("quakeDecay")->second.getFloat());
            def.setAmmos(anObstacle.find("nbAmmo")->second.getInt());
            def.setShieldPower(anObstacle.find("shieldDuration")->second.getFloat());
            def.setExplosionSoundName(anObstacle.find("explosionSound")->second.getString());
            
            if (def.getHealthModifier() < 0) {
                def.setType(MOB);
            }else if(anObstacle.find("z") != anObstacle.end()){
                def.setZ(anObstacle.find("z")->second.getFloat());
                def.setType(DECOR);
            }else if(def.getSpeedModifier() < 0){
                def.setType(SLOWER);
            }else{
                def.setType(BONUS);
            }
            def.setYSpeedFactor(anObstacle.find("ySpeedFactor")->second.getFloat());
            def.setXDelta(anObstacle.find("deltaX")->second.getFloat());
            
            if (anObstacle.find("animations") != anObstacle.end()) {
                BaconBox::Object animation = anObstacle.find("animations")->second.getArray()[0].getObject();
                def.setAnimationName(animation.find("name")->second.getString());
                def.setFps(animation.find("fps")->second.getInt());
                
                def.setFrameSize(Vector2(anObstacle.find("frameWidth")->second.getFloat(),anObstacle.find("frameHeight")->second.getFloat()));
                BaconBox::Array frames = animation.find("frames")->second.getArray();
                std::vector<unsigned int> framesVect;
                for (int i = 0; i < frames.size(); ++i) {
                    framesVect.push_back(frames[i].getInt());
                }
                def.setFrames(framesVect);
            }
            
            BaconBox::Array obstacleExplosionSet = anObstacle.find("explosions")->second.getArray();
            for (int j=0; j < obstacleExplosionSet.size(); ++j) {
                def.addExplosion(obstacleExplosionSet[j].getString());
            }
            obstacleBag.push_back(def);
        }

        Object explosionSet = (myObject.find("blasts"))->second.getObject();

        //load all explosions
        for (Object::iterator it = explosionSet.begin(); it != explosionSet.end(); ++it) {
            
            ExplosionDefinition def;
            std::string name = it->first;
            Object explosion = it->second.getObject();
            std::string image = explosion.find("image")->second.getString();
            
            ResourceManager::loadTextureRelativePath(name, image);
            def.setName(name);
            def.setForce(explosion.find("force")->second.getFloat());
            def.setForceRange(explosion.find("forceRange")->second.getFloat());
            def.setAngle(explosion.find("angle")->second.getFloat());
            def.setAngleRange(explosion.find("angleRange")->second.getFloat());
            def.setNbEmissions(explosion.find("nbEmissions")->second.getInt());
            def.setLifeSpan(explosion.find("lifeSpan")->second.getFloat());
            def.setFrameHeight(explosion.find("frameHeight")->second.getFloat());
            def.setFrameWidth(explosion.find("frameWidth")->second.getFloat());
            
            //load the animations if needed.
            if (explosion.find("animations") != explosion.end()) {
                Object anim = explosion.find("animations")->second.getArray()[0].getObject();
                Array frames = anim.find("frames")->second.getArray();
                std::vector<unsigned int> vectFrames;
                for (int i = 0; i < frames.size(); ++i) {
                    vectFrames.push_back(frames[i].getInt());
                }
                def.setAnim(new AnimationDefinition(vectFrames,(1.0/anim.find("fps")->second.getFloat()),-1));
                
            }
            explosionBag.push_back(def);
        }
        
    }
    
    void ObstacleManager::resolveCollision(){
        Player* player = static_cast<PlayState*>(Registry::playState)->getPlayer();
        std::list<Obstacle*>::iterator it;
        for (it = obstacleList.begin(); it != obstacleList.end(); it++) {
            if ((*it)->getType() != DECOR && !(*it)->isDestroyed()) {
                float radius = (*it)->getCollisionRadius();
            
                //handle the case of the rockets and the shield
                Vector2 collisionVect = (*it)->getPositionCenter() - player->getPositionCenter();
                int collisionAngle = abs((int)(collisionVect).getAngle()%180);
                bool carCollision = radius > ((*it)->getPositionCenter() - player->getPositionCenter()).getLength();
                bool destroyByRocket = radius > ((*it)->getPositionCenter() - player->getRocket()->getPositionCenter()).getLength();
                bool destroyByShield = carCollision && player->isShielded();
                
                if ((*it)->getType() == MOB && (destroyByRocket || destroyByShield)) {
                    (*it)->destroy();
                }else if((*it)->getType() == MOB && carCollision && collisionAngle < (180-SCRATCH_LIMIT_ANGLE) && collisionAngle > SCRATCH_LIMIT_ANGLE){
                    
                    collisionVect.normalize();
                    player->bounce(collisionVect.x > 0 ? -1 : 1);
                    MusicEngine::playSoundFX("scratch");
                    
                }else if(carCollision){
                    if((*it)->getAmmos()){
                        player->addRockets((*it)->getAmmos());
                    }
                    if((*it)->getShieldPower()){
                        player->setShield((*it)->getShieldPower());
                    }
                    player->applySpeedModifier((*it)->getSpeedModifier());
                    player->applyHealthModifier((*it)->getHealthModifier());
                    static_cast<PlayState*>(Registry::playState)->addPoints((*it)->getScoreBonuses());
                    
                    (*it)->destroy();
                }
            }
        }
    }
    
    void ObstacleManager::render(){
        std::list<Obstacle*>::iterator it;
        for (it = obstacleList.begin(); it != obstacleList.end(); it++) {
            (*it)->render();
        }
    }
    
    void ObstacleManager::update(float speed){
        std::list<Obstacle*>::iterator it;
        for (it = obstacleList.begin(); it != obstacleList.end(); it++) {
            float ratio = Engine::getUpdatesPerSecond()*Engine::getSinceLastUpdate();
            (*it)->setVelocity(ratio*Engine::getUpdatesPerSecond()*(*it)->getVelocityRatios()*speed);
            
            (*it)->update();
            updateObstacleOrientation(*it);
        }
        if (obstacleList.size() > 0 && obstacleList.front()->getYPosition() > Registry::playState->getCamera().getHeight()) {
            delete obstacleList.front();
            obstacleList.pop_front();
        }
    }
    
    Obstacle* ObstacleManager::addObstacle(ObstacleType type){
        
        std::vector<unsigned int> typedObstacles;
        for (unsigned int i = 0; i <obstacleBag.size(); ++i) {
            if (obstacleBag[i].getType() == type) {
                
                typedObstacles.push_back(i);
            }
        }
        int newObstacleIndex = typedObstacles[Random::getRandomInteger(0, (int)typedObstacles.size()-1)];
        std::list<std::string> explosionsNames = obstacleBag[newObstacleIndex].getExplosionNames();
        std::list<const ExplosionDefinition*> explosionsDef;
        int i = 0;
        while (explosionsNames.size() != 0 && i < explosionBag.size()) {
            //TODO :: FIX THIS ITS SO NOT OPTIMAL!!!
            if (explosionBag[i].getName() == explosionsNames.front()) {
                explosionsDef.push_back(&(explosionBag[i]));
                i = 0;
                explosionsNames.pop_front();
                
            }else{
                ++i;
            }
        }
        obstacleList.push_back(new Obstacle(obstacleBag[newObstacleIndex],explosionsDef));
        return obstacleList.back();
    }
    
    void ObstacleManager::updateObstacleOrientation(Obstacle* obs){
        //change the direction of the obstacle if outside borders
        const TileInfo* tile = ((PlayState*)(Registry::playState))->getCurrentTileInfo(obs);
        
        float obsRelativePosition = (obs)->getCentroid().x - Registry::playState->getCamera().getCentroid().x;
        if (tile) {
            
            if((tile->rightEnnemiLimit < obsRelativePosition && (obs)->getVelocityRatios().x > 0) ||
               (tile->leftEnnemiLimit > obsRelativePosition && (obs)->getVelocityRatios().x < 0)){
                (obs)->swapDeltaX();
            }
            //if the obstacle is between the medians go to the direction of the Relative position
            if (tile->medians.first < obsRelativePosition && tile->medians.second > obsRelativePosition) {
                if ((obsRelativePosition < 0 && (obs)->getVelocityRatios().x > 0) ||
                    (obsRelativePosition > 0 && (obs)->getVelocityRatios().x < 0)) {
                    (obs)->swapDeltaX();
                }
            }
        }
    }
}