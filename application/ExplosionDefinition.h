//
//  ExplosionDefinition.h
//  BaconBoxApp
//
//  Created by PHILIPPE DUBE-TREMBLAY on 2012-09-25.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#ifndef EXPLOSIONDEFINITION_H
#define EXPLOSIONDEFINITION_H


#include <BaconBox.h>

namespace StreetMadness {
    class ExplosionDefinition{
    public:
        ExplosionDefinition();
        ~ExplosionDefinition(){}
        
        const std::string& getName() const { return name; }
        void setName(std::string newName) { name = newName; }
        float getForce() const {return force; }
        void setForce(float newForce) { force = newForce; }
        float getForceRange() const {return forceRange; }
        void setForceRange(float newForceRange) { forceRange = newForceRange; }
        float getAngle() const {return angle; }
        void setAngle(float newAngle) { angle = newAngle; }
        float getAngleRange() const {return angleRange; }
        void setAngleRange(float newAngleRange) { angleRange = newAngleRange; }
        int getNbEmissions() const {return nbEmissions; }
        void setNbEmissions(int newNbEmissions) { nbEmissions = newNbEmissions; }
        float getLifeSpan() const {return lifeSpan; }
        void setLifeSpan(float newLifeSpan) { lifeSpan = newLifeSpan; }
        const BaconBox::AnimationDefinition* getAnim() const { return anim; }
        void setAnim(BaconBox::AnimationDefinition* newAnim) { anim = newAnim; }
        float getFrameWidth() const {return frameWidth; }
        void setFrameWidth(float newFrameWidth) { frameWidth = newFrameWidth; }
        float getFrameHeight() const {return frameHeight; }
        void setFrameHeight(float newFrameHeight) { frameHeight = newFrameHeight; }
        
    private:
        std::string name;
        float force;
        float forceRange;
        float angle;
        float angleRange;
        float frameWidth;
        float frameHeight;
        int nbEmissions;
        float lifeSpan;
        BaconBox::AnimationDefinition* anim;
    };
}
#endif
