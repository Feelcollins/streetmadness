//
//  MenuState.cpp
//  BaconBoxApp
//
//  Created by PHILIPPE DUBE-TREMBLAY on 2012-10-30.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#include "MenuState.h"
#include "Registry.h"
#include <iostream.h>
#include <fstream.h>

using namespace BaconBox;

namespace StreetMadness {
    
    MenuState::MenuState(const std::string &newName) : State(newName){
        InputManager::getDefaultPointer()->connectButtonRelease(this, &MenuState::onClick);
        
        cameraEasing.setDuration(1);
        cameraEasing.setEase(Ease::OUT_EXPO);
        
        ResourceManager::loadTextureRelativePath("menuBG", "Fond.png");
        ResourceManager::loadTextureRelativePath("OptionsBG", "OptionsBG.png");
        ResourceManager::loadTextureRelativePath("creditsBG", "CreditsBG.png");
        ResourceManager::loadTextureRelativePath("HelpBG1", "HowToBG.png");
        ResourceManager::loadTextureRelativePath("HelpBG2", "helpH.png");
        ResourceManager::loadTextureRelativePath("ScoresBG", "ScoresBG.png");
        
        ResourceManager::loadTextureRelativePath("btnHelp", "Bouton-Help.png");
        ResourceManager::loadTextureRelativePath("btnPlay", "Bouton-Play.png");
        ResourceManager::loadTextureRelativePath("btnScore", "Bouton-Scores.png");
        ResourceManager::loadTextureRelativePath("btnBack", "Bouton-Back.png");
        ResourceManager::loadTextureRelativePath("btnMore", "Bouton-More.png");
        ResourceManager::loadTextureRelativePath("btnOptions", "Bouton-Options.png");
        ResourceManager::loadTextureRelativePath("btnCredits", "Bouton-Credits.png");
        
        ResourceManager::loadTextureRelativePath("frame", "frame.png");
        ResourceManager::loadTextureRelativePath("iPod1", "iPod-1.png");
        ResourceManager::loadTextureRelativePath("iPod2", "iPod-2.png");
        
        ResourceManager::loadMusicRelativePath("menuMusic", "menuMusic.wav");
        ResourceManager::loadSoundRelativePath("click", "buttonClick.wav");
        ResourceManager::loadSoundRelativePath("start", "carStart.wav");
        
        targa = new Font("Targa", ResourcePathHandler::getResourcePathFor("Targa.ttf"));
        
        optionFrame = new Sprite("frame");
        optionIPhone1 = new Sprite("iPod1");
        optionIPhone2 = new Sprite("iPod2");
        
        btnHelp = new Sprite("btnHelp",Vector2(),Vector2(122,72));
        btnPlay = new Sprite("btnPlay",Vector2(),Vector2(122,72));
        btnScore = new Sprite("btnScore",Vector2(),Vector2(122,72));
        btnMore = new Sprite("btnMore",Vector2(),Vector2(122,72));
        btnOptions = new Sprite("btnOptions",Vector2(),Vector2(122,72));
        btnCredits = new Sprite("btnCredits",Vector2(),Vector2(122,72));
        btnBack = new Sprite("btnBack",Vector2(-500,-500),Vector2(122,72));
        initBGs();
        
        add(btnHelp);
        add(btnPlay);
        add(btnScore);
        add(btnBack);
        add(btnMore);
        add(btnOptions);
        add(btnCredits);
        
        add(optionFrame);
        add(optionIPhone1);
        add(optionIPhone2);
        
    }
    MenuState::~MenuState(){
        
    }
    
    void MenuState::onGetFocus(){
        MusicEngine::playBackgroundMusic("menuMusic");
        init();
    }
    
    void MenuState::init(){
        
        initBtns();
        initContent();
        initScore();
    }
    
    void MenuState::update(){
        camera.setPosition(cameraEasing.getValue());
        if (Registry::controlHorizontal) {
            optionFrame->setPosition(optionIPhone1->getPosition());
        }else{
            optionFrame->setPosition(optionIPhone2->getPosition());
        }
    }
    
    void MenuState::onClick(PointerButtonSignalData data){
        
        if (btnPlay->getAxisAlignedBoundingBox().overlaps(camera.screenToWorld(data.getPosition()))) {
            Engine::playState("PlayState");
            MusicEngine::playSoundFX("start");
        }
        if (btnOptions->getAxisAlignedBoundingBox().overlaps(camera.screenToWorld(data.getPosition()))) {
            switchScreen(options);
            MusicEngine::playSoundFX("click");
        }else if (btnScore->getAxisAlignedBoundingBox().overlaps(camera.screenToWorld(data.getPosition()))) {
            switchScreen(score);
            MusicEngine::playSoundFX("click");
        }else if (btnHelp->getAxisAlignedBoundingBox().overlaps(camera.screenToWorld(data.getPosition()))) {
            switchScreen(helpMarkers);
            MusicEngine::playSoundFX("click");
        }else if (btnMore->getAxisAlignedBoundingBox().overlaps(camera.screenToWorld(data.getPosition()))) {
            switchScreen(helpTilt);
            MusicEngine::playSoundFX("click");
        }else if (btnBack->getAxisAlignedBoundingBox().overlaps(camera.screenToWorld(data.getPosition()))) {
            switchScreen(main);
            MusicEngine::playSoundFX("click");
        }else if (optionIPhone1->getAxisAlignedBoundingBox().overlaps(camera.screenToWorld(data.getPosition()))) {
            Registry::controlHorizontal = true;
            
            std::string path = ResourcePathHandler::getResourcePathFor("Config.txt");
            
            JsonSerializer serializer;
            BaconBox::Value writer;
            writer["ControlHorizontal"] = true;
            serializer.writeToFile(path, writer);
            
        }else if (optionIPhone2->getAxisAlignedBoundingBox().overlaps(camera.screenToWorld(data.getPosition()))) {
            Registry::controlHorizontal = false;
            
            std::string path = ResourcePathHandler::getResourcePathFor("Config.txt");
            
            JsonSerializer serializer;
            BaconBox::Value writer;
            writer["ControlHorizontal"] = false;
            serializer.writeToFile(path, writer);
        }
    }
    
    Vector2 MenuState::getBtnPos(float xRatio){
        
        return Vector2(500,xRatio*camera.getHeight()-btnPlay->getHeight()/2) + camera.getPosition();
    }
    
    void MenuState::switchScreen(MenuScreen screen) {
        switch (screen) {
            case main:
                cameraEasing.setStartValue(camera.getPosition());
                cameraEasing.setEndValue(Vector2(0,0));
                cameraEasing.start();
                break;
            case options:
                cameraEasing.setStartValue(camera.getPosition());
                cameraEasing.setEndValue(Vector2(camera.getWidth(),0));
                cameraEasing.start();
                btnBack->setPosition(getBtnPos(3.5/4.0) + cameraEasing.getEndValue());
                break;
            case score:
                cameraEasing.setStartValue(camera.getPosition());
                cameraEasing.setEndValue(Vector2(0,camera.getHeight()));
                cameraEasing.start();
                btnBack->setPosition(getBtnPos(3.5/4.0) + cameraEasing.getEndValue());
                break;
            case helpMarkers:
                cameraEasing.setStartValue(camera.getPosition());
                cameraEasing.setEndValue(Vector2(0,-camera.getHeight()));
                cameraEasing.start();
                break;
            case helpTilt:
                cameraEasing.setStartValue(camera.getPosition());
                cameraEasing.setEndValue(Vector2(0,-2*camera.getHeight()));
                cameraEasing.start();
                btnBack->setPosition(getBtnPos(3.5/4.0) - Vector2(50,camera.getHeight()));
                break;
        }
    }
    
    void MenuState::initBGs(){
        
        backgrounds.push_back(new Sprite("menuBG"));
        backgrounds.back()->setAngle(90);
        backgrounds.back()->setScaling(2,2);
        backgrounds.back()->setPosition(camera.getPosition());
        add(backgrounds.back());
        backgrounds.push_back(new Sprite("OptionsBG"));
        backgrounds.back()->setAngle(90);
        backgrounds.back()->setScaling(2,2);
        backgrounds.back()->setPosition(camera.getWidth(),0);
        add(backgrounds.back());
        backgrounds.push_back(new Sprite("ScoresBG"));
        backgrounds.back()->setAngle(90);
        backgrounds.back()->setScaling(2,2);
        backgrounds.back()->setPosition(0,camera.getHeight());
        add(backgrounds.back());
        backgrounds.push_back(new Sprite("HelpBG1"));
        backgrounds.back()->setAngle(90);
        backgrounds.back()->setScaling(2,2);
        backgrounds.back()->setPosition(0,-camera.getHeight());
        add(backgrounds.back());
        backgrounds.push_back(new Sprite("HelpBG2"));
        backgrounds.back()->setAngle(90);
        backgrounds.back()->setScaling(2,2);
        backgrounds.back()->setPosition(0,-2*camera.getHeight());
        add(backgrounds.back());
    }
    
    void MenuState::initBtns(){
        
        btnPlay->setAngle(90);
        btnScore->setAngle(90);
        btnOptions->setAngle(90);
        btnCredits->setAngle(90);
        btnHelp->setAngle(90);
        btnBack->setAngle(90);
        btnMore->setAngle(90);
        
        btnPlay->setScaling(Vector2(1.5,1.5));
        btnScore->setScaling(Vector2(1.5,1.5));
        btnOptions->setScaling(Vector2(1.5,1.5));
        btnCredits->setScaling(Vector2(1.5,1.5));
        btnHelp->setScaling(Vector2(1.5,1.5));
        btnBack->setScaling(Vector2(1.5,1.5));
        btnMore->setScaling(Vector2(1.5,1.5));
        
        
        btnPlay->setPosition(getBtnPos(3.5/4.0));
        btnScore->setPosition(getBtnPos(2.5/4.0));
        btnOptions->setPosition(getBtnPos(1.5/4.0));
        btnHelp->setPosition(getBtnPos(0.5/4.0));
        
        btnMore->setPosition(getBtnPos(0.5/4.0) + Vector2(0,-camera.getHeight()));
        btnCredits->setPosition(getBtnPos(3.5/4.0) + Vector2(-camera.getWidth(),0));
    }
    
    void MenuState::initContent(){
        
        optionFrame->setAngle(90);
        optionIPhone1->setAngle(90);
        optionIPhone2->setAngle(90);
        
        optionFrame->setScaling(Vector2(1.5,1.5));
        optionIPhone1->setScaling(Vector2(1.5,1.5));
        optionIPhone2->setScaling(Vector2(1.5,1.5));
        
        optionIPhone1->setPosition(Vector2(camera.getWidth(), 0) + Vector2(camera.getWidth()/2-optionIPhone1->getWidth(),camera.getHeight()/2));
        optionIPhone2->setPosition(optionIPhone1->getPosition() - Vector2(0,1.5*optionIPhone2->getHeight()));
        optionFrame->setPosition(optionIPhone1->getPosition());
        
        
        std::string path = ResourcePathHandler::getResourcePathFor("Config.txt");
        
        JsonSerializer serializer;
        BaconBox::Value reader;
        serializer.readFromFile(path, reader);
        Object myObject = reader.getObject();
        Registry::playerName = myObject.find("PlayerName")->second.getString();
        Registry::controlHorizontal = myObject.find("ControlHorizontal")->second.getBool();

        
        
    }
    
    void MenuState::initScore(){
        
        std::string path = ResourcePathHandler::getResourcePathFor("Scores.txt");
        std::string name;
        std::string pos;
        std::string score;
        std::stringstream aStream;
        std::ifstream myFile (path.c_str());
        std::pair<int, std::string> scoreEntry;
        targa->setPixelSize(35);
        
        while(_scoresTexts.size() != 0){
            _scoresTexts.front()->setToBeDeleted(true);
            _scoresTexts.erase(_scoresTexts.begin());
        }
        
        for(int i = 0; i < 10;++i){
            // écrire une ligne.
            aStream << i+1 << ". ";
            pos = aStream.str();
            aStream.str("");
            
            Text* headLine = new Text(targa,TextAlignment::LEFT);
            headLine->setColor(Color::WHITE);
            headLine->setAngle(90);
            headLine->setText(pos);
            headLine->setPosition(Vector2(175+35*i,710 + camera.getHeight()));
            
            while (myFile.peek() != 9) {
                aStream << (char)(myFile.get());
            }
            name = aStream.str();
            aStream.str("");
            myFile.get();
            char next = (char)(myFile.peek());
            while (myFile.peek() != 10 && !myFile.eof()) {
                aStream << (char)(myFile.get());
                next = (char)(myFile.peek());
            }
            
            score = aStream.str();
            aStream.str("");
            myFile.get();
            Text* leftLine = new Text(targa,TextAlignment::LEFT);
            Text* rightLine = new Text(targa,TextAlignment::RIGHT);
            leftLine->setColor(Color::WHITE);
            rightLine->setColor(Color::WHITE);
            leftLine->setAngle(90);
            rightLine->setAngle(90);
            leftLine->setText(name);
            rightLine->setText(score);
            leftLine->setPosition(Vector2(175+35*i,700 + camera.getHeight() - leftLine->getHeight()));
            rightLine->setPosition(Vector2(175+35*i,250 + camera.getHeight()));
            
            scoreEntry.first = ::atoi(score.c_str());
            scoreEntry.second = name;
            Registry::topTen.insert(scoreEntry);
            _scoresTexts.push_back(leftLine);
            _scoresTexts.push_back(rightLine);
            _scoresTexts.push_back(headLine);
            add(leftLine);
            add(rightLine);
            add(headLine);
        }
    }
}


