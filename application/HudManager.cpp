//
//  Hud.cpp
//  BaconBoxApp
//
//  Created by PHILIPPE DUBE-TREMBLAY on 2012-07-28.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#include "HudManager.h"

using namespace BaconBox;

namespace StreetMadness {
    
    HudManager::HudManager(){
        
        ResourceManager::loadTextureRelativePath("life", "healthBar.png");
        ResourceManager::loadTextureRelativePath("ammo", "ammo.png");
        ResourceManager::loadTextureRelativePath("mileCounter", "milesCounter.png");
        ResourceManager::loadTextureRelativePath("mileCounterGlass", "milesCounterGlass.png");
        ResourceManager::loadTextureRelativePath("lifeContainer", "Cadre.png");
        ResourceManager::loadTextureRelativePath("numbers", "Numbers2.png");
        
        _mileCounter = new Sprite("mileCounter");
        _mileCounterGlass = new Sprite("mileCounterGlass");
        
        for(int i =0; i<NB_DIGITS_MILES;++i){
            _miles.push_back(new Sprite("numbers",Vector2(),Vector2(42,50)));
            _miles[i]->addAnimation("main", -1,-1,10,0,1,2,3,4,5,6,7,8,9);
        }
        _lifeBar = new Sprite("lifeContainer");
        _life = new Sprite("life");
        _ammo = new Sprite("ammo");
    }
    HudManager::~HudManager(){
        delete _life;
        delete _lifeBar;
        delete _ammo;
        while(_miles.size()){
            delete _miles.back();
            _miles.erase((--_miles.end()));
        }
    }
    void HudManager::init(){
        
        _mileCounter->setPosition(0,0);
        _mileCounterGlass->setPosition(0,0);
        
        for(int i =0; i<NB_DIGITS_MILES;++i){
            _miles[i]->startAnimation("main");
            _miles[i]->setCurrentFrame(0);
            _miles[i]->setScaling(0.2,0.25);
            _miles[i]->setAngle(90);
            _miles[i]->setPosition(7,11+(11*i));
        }
        
        _lifeBar->setPosition(0,Registry::playState->getCamera().getHeight()-_lifeBar->getHeight());
        _life->setScaling(Vector2(HEALTHBAR_Y_VOLUME,0.5));
        _life->setXPosition(HEALTHBAR_X_OFFSET);
        
        
        _ammo->setScaling(Vector2(AMMOBAR_Y_VOLUME,0.5));
        _ammo->setXPosition(AMMOBAR_X_OFFSET);
    }
    
    void HudManager::render(){
        
        _mileCounter->render();
        for(int i =0; i<NB_DIGITS_MILES;++i){
            _miles[i]->render();
        }
        _mileCounterGlass->render();
        
        _life->render();
        _ammo->render();
        _lifeBar->render();
    }
    
    void HudManager::update(int score, int lifePourcent, int ammo){
        //the addition of 0.5 prevent the scaling to get to 0
        //TODO : fix this in a better way
        
        _life->setYScaling(HEALTHBAR_X_VOLUME*lifePourcent/100*0.5 +0.5);

        _life->setYPosition(_lifeBar->getYPosition()+HEALTHBAR_Y_OFFSET-_life->getHeight());
        _lifeBar->update();
        _mileCounter->update();
        int rest = score;
        for(int i =NB_DIGITS_MILES-1; i >= 0;--i){
            int power = ((int)pow(10, i));
            int frame = (rest- rest%power)/power;
            rest = rest%power;
            _miles[i]->setCurrentFrame(frame);
            
        }
        _mileCounterGlass->update();
        _life->update();
        
        _ammo->setYScaling(AMMOBAR_X_VOLUME*ammo/100*0.5 +0.5);
        _ammo->setYPosition(_lifeBar->getYPosition()+AMMOBAR_Y_OFFSET-_ammo->getHeight());
        _ammo->update();
    }
    void HudManager::calculateLifeTransparency(Sprite* player){
        if (player->getAxisAlignedBoundingBox().overlaps(_lifeBar->getAxisAlignedBoundingBox())) {
            _lifeBar->setAlpha(128);
            _life->setAlpha(128);
            _ammo->setAlpha(50);
        }else{
            _lifeBar->setAlpha(255);
            _life->setAlpha(255);
            _ammo->setAlpha(255);
        
        }
    }
    
}