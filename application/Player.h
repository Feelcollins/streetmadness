//
//  Player.h
//  BaconBoxApp
//
//  Created by Philippe Dubé-Tremblay on 12-04-24.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#ifndef SM_PLAYER_H
#define SM_PLAYER_H

#include <BaconBox.h>
#include "Registry.h"

namespace StreetMadness {
    const float START_CAR_SPEED = 5.0;
    const float MAX_CAR_SPEED = 10.0;
    const float MIN_CAR_SPEED = 3.0;
    const float STEER_SPEED = 2.0;
    const float ROTATION_FORCE = -90.0;
    const float MAX_ROTATION = 0.25;
    const float CAR_ACCELERATION = 0.25;
    const float CAR_ACCELERATION_TICK = 1;
    const float DEVICE_ACCEL_SMOOTHINGFILTER = 0.07;
    const float MARGIN_REBOUND = 0.1;
    const float MAX_LIFE = 100.0;
    const int MAX_AMMO = 3.0;
	class Player : public BaconBox::Sprite {
    public:
        Player();
        void init();
        ~Player();
        void update();
        void render();
        virtual void rotateFromPoint(float rotationAngle, const BaconBox::Vector2 &rotationPoint);
        virtual void scaleFromPoint(float xScaling, float yScaling, const BaconBox::Vector2 &fromPoint);
        virtual void move(float xDelta, float yDelta);
        void applyHealthModifier(float healthModifier);
        void healPlayer(float health);
        float getLifeInPourcent() const;
        void applySpeedModifier( float speedModifier);
        float getSpeed() const;
        float getRocketsInPourcent() const;
        BaconBox::Sprite* getRocket() const;
        void addRockets(int nbr);
        bool shootRocket();
        void setShield(float time);
        bool isShielded();
        bool isBouncing();
        void bounce(float force);
        float getTurning(){return _turningEase;}
        void turn(bool right, float intensity = 0.1);
        bool isDead(){return dead;}
        
    private:
        BaconBox::SpriteEmitter* exhaust;
		std::vector<BaconBox::SpriteEmitter*> shieldExhausts;
		BaconBox::Sprite* carShield;
		BaconBox::Sprite* rocketLauncher;
		BaconBox::Sprite* rocket;
        BaconBox::GraphicElement<BaconBox::Collidable>* fume;
        bool dead;
        unsigned int _bouncingDelay;
        bool _isTurnning;
        float _turningEase;
        float life;
        int rocketsLeft;
        float carSpeed;
        double lastAccelerationTime;
        double shieldTimer;
        float lastAccelerometerValue;
        void addSpeed(float SpeedModifier);
        void setSpeed(float newSpeed);
        void updateSpeed(double time);
        void updateInputsEffect();
        void easeTurning();
        void turn();    
	};
}


#endif
