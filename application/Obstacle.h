//
//  Player.h
//  BaconBoxApp
//
//  Created by Philippe Dubé-Tremblay on 12-04-24.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#ifndef SM_OBSTACLE_H
#define SM_OBSTACLE_H

#include "ObstacleDefinition.h"
#include "ExplosionDefinition.h"
#include "Registry.h"

namespace StreetMadness {

    class Obstacle : public BaconBox::Sprite {
	public:
        static const char* HEALTH_INDICATOR_PATH;
        static const char* DANGER_INDICATOR_PATH;
        static const char* OBSTACLE_INDICATOR_PATH;
        static const char* SHIELD_INDICATOR_PATH;
        static const char* ROCKET_INDICATOR_PATH;
		static const int INDICATOR_FRAME_WIDTH = 22;
		static const int INDICATOR_FRAME_HEIGHT = 23;
        
        Obstacle(const ObstacleDefinition& def, std::list<const ExplosionDefinition*> explosions);
        ~Obstacle();
        void destroy();
        void update();
        void render();
        const bool isDestroyed(){ return _isDestroyed; }
        const BaconBox::Vector2& getVelocityRatios() const { return velocityRatios; }
        void swapDeltaX() { velocityRatios.x = -velocityRatios.x; }
        const float getCollisionRadius() const { return _definition->getCollisionRadius(); }
        const float getScratchRadius() const { return _definition->getScratchRadius(); }
        const std::string& getName() const { return _definition->getName(); }
        const ObstacleType getType() const { return _definition->getType(); }
        float getHealthModifier() const { return _definition->getHealthModifier(); }
        float getSpeedModifier() const { return _definition->getSpeedModifier(); }
        float getScoreBonuses() const { return _definition->getScoreBonus(); }
        float getAmmos() const { return _definition->getAmmos(); }
        float getShieldPower() const { return _definition->getShieldPower(); }
        
    private:
        const ObstacleDefinition* _definition;
        bool _isDestroyed;
        bool _veloIsApplyToParticles;
        BaconBox::Vector2 velocityRatios;
        void ApplyVelocityToParticles();
        BaconBox::Sprite* marker;
        std::list<BaconBox::SpriteEmitter*> _explosions;
    
    };
    
}
#endif /* defined(__BaconBoxApp__Obstacle__) */
