//
//  Header.h
//  BaconBoxApp
//
//  Created by Philippe Dubé-Tremblay on 12-05-10.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#ifndef SM_OBSTACLEMANAGER_H
#define SM_OBSTACLEMANAGER_H

#include <BaconBox.h>
#include "Obstacle.h"
#include "ObstacleDefinition.h"
#include "ExplosionDefinition.h"
#include "Registry.h"

namespace StreetMadness {
    const float SCRATCH_LIMIT_ANGLE = 30.0;
	class ObstacleManager{
    public:
        ObstacleManager();
        ~ObstacleManager();
        void init();
        void update(float speed);
        void render();
        Obstacle* addObstacle(ObstacleType type);
        void resolveCollision();
        void updateObstacleOrientation(Obstacle* obs);
    private:
        float pourcentObstacleSpawning;
        void loadData();
        std::list<Obstacle*> obstacleList;
        std::vector<ObstacleDefinition> obstacleBag;
        std::vector<ExplosionDefinition> explosionBag;
	};
}


#endif
