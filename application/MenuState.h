//
//  MenuState.h
//  BaconBoxApp
//
//  Created by PHILIPPE DUBE-TREMBLAY on 2012-10-30.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#ifndef SM_MENUSTATE_H
#define SM_MENUSTATE_H

#include <BaconBox.h>

namespace StreetMadness {
	enum MenuScreen {
		main,score,helpMarkers,helpTilt,credits,options
	};
    class MenuState : public BaconBox::State {
    public:
        MenuState(const std::string &newName = std::string("MenuState"));
        ~MenuState();
        void init();
        void onGetFocus();
        void update();
    private:
        void onClick(BaconBox::PointerButtonSignalData data);
        BaconBox::Vector2 getBtnPos(float xRatio);
        void switchScreen(MenuScreen screen);
        void initBGs();
        void initBtns();
        void initContent();
        void initScore();
        
        BaconBox::Sprite* btnHelp;
        BaconBox::Sprite* btnPlay;
        BaconBox::Sprite* btnScore;
        BaconBox::Sprite* btnBack;
        BaconBox::Sprite* btnMore;
        BaconBox::Sprite* btnOptions;
        BaconBox::Sprite* btnCredits;
        
        BaconBox::Sprite* optionFrame;
        BaconBox::Sprite* optionIPhone1;
        BaconBox::Sprite* optionIPhone2;
        
        std::list<BaconBox::Text*> _scoresTexts;
        
        BaconBox::Font* targa;
        
        std::list<BaconBox::Sprite*> backgrounds;
        BaconBox::Tween<BaconBox::Vector2> cameraEasing;
        
	};
}

#endif
