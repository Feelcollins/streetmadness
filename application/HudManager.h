//
//  Hud.h
//  BaconBoxApp
//
//  Created by PHILIPPE DUBE-TREMBLAY on 2012-07-28.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//


#ifndef SM_HUDMANAGER_h
#define SM_HUDMANAGER_h

#include <BaconBox.h>
#include "Registry.h"

namespace StreetMadness {
    
    const float HEALTHBAR_Y_OFFSET = 142.0;
    const float HEALTHBAR_X_OFFSET = 6.0;
    const float HEALTHBAR_X_VOLUME = 129.0;
    const float HEALTHBAR_Y_VOLUME = 14.0;
    
    const float AMMOBAR_Y_OFFSET = 105.0;
    const float AMMOBAR_X_OFFSET = 37.0;
    const float AMMOBAR_Y_VOLUME = 5.0;
    const float AMMOBAR_X_VOLUME = 70.0;
    
    const int NB_DIGITS_MILES = 7;
    
    class HudManager{
    public:
        HudManager();
        ~HudManager();
        void init();
        void render();
        void update(int score, int lifeLeft, int ammo);
        void calculateLifeTransparency(BaconBox::Sprite*);
    private:
        std::vector<BaconBox::Sprite*> _miles;
        BaconBox::Sprite* _mileCounter;
        BaconBox::Sprite* _mileCounterGlass;
        
        BaconBox::Sprite* _lifeBar;
        BaconBox::Sprite* _life;
        
        BaconBox::Sprite* _ammo;
        
        
    };
}

#endif
