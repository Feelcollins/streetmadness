//
//  Player.cpp
//  BaconBoxApp
//
//  Created by Philippe Dubé-Tremblay on 12-04-24.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#include "Player.h"
#include "PlayState.h"

using namespace BaconBox;

namespace StreetMadness {
    Player::Player() : Sprite("player"){
        
        //exhaust initialisation
        fume = new GraphicElement<Collidable>("exhaust");
        exhaust = new SpriteEmitter(fume);
        exhaust->setNbParticlesToShoot(-1);
        exhaust->setMaximumNbParticles(15);
        exhaust->setShootingAngle(180);
        exhaust->setShootingAngleVariance(13);
        exhaust->setShootingForce(25);
        exhaust->setShootingForceVariance(5);
        exhaust->setTimeBetweenSpawns(0.1);
        exhaust->setLifeSpan(-1);
        ParticlePhase aPhase = ParticlePhase();
        aPhase.phaseDuration = 1;
        aPhase.scalingPerSecond = Vector2(0.1,0.1);
        aPhase.alphaPerSecond = -20;
        aPhase.anglePerSecondVariance = 360;
        exhaust->getPhases().push_back(aPhase);
        rocketLauncher = new Sprite("rocketLaucher");
        
        carShield = new Sprite("carShield", Vector2(), Vector2(25.0, 59.0));

        carShield->addAnimation("mainAnim",0.2,-1,3,0,1,2);
        
        rocket = new Sprite("rocket");
        init();
    }
    
    void Player::init(){
        dead = false;
        _bouncingDelay = 0;
        shieldTimer = 0;
        rocketsLeft = 0;
        life = MAX_LIFE;
        carSpeed = MIN_CAR_SPEED;
        lastAccelerometerValue = 0;
        
        
        rocket->setYVelocity(-500);
        carShield->startAnimation("mainAnim");
        rocketLauncher->setPosition(getPositionCenter() - rocketLauncher->getSize()/2);
        rocketLauncher->setVisible(false);
        exhaust->start();
        exhaust->setPosition(getPosition()+Vector2(getWidth()/2, getHeight())-Vector2(fume->getWidth()/2,fume->getHeight()));
    }
    
    Player::~Player(){
        delete exhaust;
        delete carShield;
        //delete shieldExhausts[0];
    }
    
    
    void Player::update(){
        double time = Engine::getSinceLastUpdate();
        updateSpeed(time);
        
        //updateInputsEffect();
        
        //update Shield's time left
        if (shieldTimer > 0) {
            shieldTimer -= time;
            if (shieldTimer < 0) {
                MusicEngine::playBackgroundMusic("gameMusic");
            }
        }
        if (isBouncing()) {
            --_bouncingDelay;
        }
        Sprite::update();
        exhaust->update();
        rocketLauncher->update();
        carShield->update();
        rocket->update();
        turn();
        easeTurning();
    }
    
    void Player::updateSpeed(double time){
        lastAccelerationTime += time;
        if (lastAccelerationTime >= CAR_ACCELERATION_TICK) {
            addSpeed(CAR_ACCELERATION);
            lastAccelerationTime = 0;
        }
        
    }
    void Player::updateInputsEffect(){
        float smoothAccelerometerValue;
        float accelerometerValue = 0;
        float screenWidth = Registry::playState->getCamera().getWidth();
        
        //////////////////////////////////////////////////////////////////////////////
        //TODO : Verify if the game runs on iPhone and the orientation of the iPhone.
        //TODO : Manage other types of inputs.
        //////////////////////////////////////////////////////////////////////////////
        
        smoothAccelerometerValue = accelerometerValue * DEVICE_ACCEL_SMOOTHINGFILTER + lastAccelerometerValue * (1 - DEVICE_ACCEL_SMOOTHINGFILTER);
        
        setAngle(smoothAccelerometerValue * ROTATION_FORCE);
        setXPosition(getXPosition()+STEER_SPEED * carSpeed * lastAccelerometerValue);
        // We make sure the player's car doesn't go out of the screen.
        if (getXPosition() < 0 || getXPosition() + getWidth() > screenWidth) {
            bounce(getXPosition() < 0 ? 1 : -1);
        }
    }
    
    void Player::render(){
        exhaust->render();
        Sprite::render();
        if (isShielded()) {
            carShield->render();
        }
        if (rocketsLeft > 0) {
            rocketLauncher->render();
        }
        rocket->render();
    }
    void Player::rotateFromPoint(float rotationAngle, const Vector2 &rotationPoint) {
        rocketLauncher->rotateFromPoint(rotationAngle,rotationPoint);
        carShield->rotateFromPoint(rotationAngle,rotationPoint);
        Sprite::rotateFromPoint(rotationAngle,rotationPoint);
    }
    
    void Player::scaleFromPoint(float xScaling, float yScaling, const BaconBox::Vector2 &fromPoint){
        exhaust->setPosition(getPosition()+Vector2(getWidth()/2, getHeight())-Vector2(fume->getWidth()/4,fume->getHeight()));
    }
    
    void Player::move(float xDelta, float yDelta){
        exhaust->move(xDelta,yDelta);
        rocketLauncher->move(xDelta,yDelta);
        carShield->move(xDelta,yDelta);
        Sprite::move(xDelta, yDelta);
    }
    
    void Player::addSpeed(float speedModifier){
        setSpeed(carSpeed+speedModifier);
    }
    
    void Player::setSpeed(float newSpeed){
        if (newSpeed > MAX_CAR_SPEED) {
            carSpeed = MAX_CAR_SPEED;
        }else if(newSpeed < MIN_CAR_SPEED) {
            carSpeed = MIN_CAR_SPEED;
        }else{
            carSpeed = newSpeed;
        }
    }
    
    void Player::applyHealthModifier(float healthModifier){
        life = std::min(life + healthModifier, MAX_LIFE);
        if (life <= 0) {
            life = 0;
            dead = true;
        }
    }
    void Player::applySpeedModifier(float speedModifier){
        carSpeed = std::max(std::min(carSpeed + speedModifier, MAX_CAR_SPEED),MIN_CAR_SPEED);
    }
    
    void Player::healPlayer(float healing){
    }
    
    float Player::getLifeInPourcent() const{
        return life/MAX_LIFE*100;
    }
    
    float Player::getRocketsInPourcent() const{
        return rocketsLeft*100/MAX_AMMO;
    }
    
    Sprite* Player::getRocket() const{
        return rocket;
    }
    
    float Player::getSpeed() const{
        return carSpeed;
    }
    
    void Player::turn(){
        
        // float smoothAccelValue = intensity * DEVICE_ACCEL_SMOOTHINGFILTER + lastAccelerometerValue * (1 - DEVICE_ACCEL_SMOOTHINGFILTER);
        // lastAccelerometerValue = std::max(std::min(smoothAccelValue ,MAX_ROTATION), -MAX_ROTATION);
        
        lastAccelerometerValue = std::max(std::min(_turningEase/4 ,MAX_ROTATION), -MAX_ROTATION);
        
        float screenWidth = Registry::playState->getCamera().getWidth();
        if ((getXPosition() < 0 || getXPosition() + getWidth() > screenWidth) && !isBouncing()) {
            if(lastAccelerometerValue > 0){
                setXPosition(screenWidth - getWidth());
                bounce(-1);
            }else{
                setXPosition(0);
                bounce(1);
            }
        }
        setAngle(lastAccelerometerValue * ROTATION_FORCE);
        
        if (isShielded()) {
            setXPosition(getXPosition()+STEER_SPEED * MAX_CAR_SPEED * lastAccelerometerValue);
        }else{
            setXPosition(getXPosition()+STEER_SPEED * carSpeed * lastAccelerometerValue);
        }
    }
    void Player::addRockets(int nbr){
        rocketsLeft = std::min(nbr+rocketsLeft, MAX_AMMO);
        rocketLauncher->setVisible(true);
    }
    bool Player::shootRocket(){
        if (rocketsLeft > 0 && rocket->getYPosition() < 0) {
            --rocketsLeft;
            MusicEngine::playSoundFX("rocketLauch");
            MusicEngine::playSoundFX("rocketLauch2");
            rocket->setPosition(rocketLauncher->getPositionCenter() - rocket->getSize()/2);
            return true;
        }
        return false;
    }
    void Player::setShield(float newTime){
        shieldTimer = newTime;
        //TODO: start the shield animation and the music
        MusicEngine::playBackgroundMusic("musicShield");
    }
    bool Player::isShielded(){
        return shieldTimer > 0;
    }
    bool Player::isBouncing(){
        return !_bouncingDelay == 0;
    }
    void Player::bounce(float force){
        if (_bouncingDelay == 0) {
            _turningEase = force;
            _bouncingDelay = 10;
        }
    }
    void Player::turn(bool right, float intensity){
        _turningEase = _turningEase + ( right ? 0.1 : -0.1);
    }
    void Player::easeTurning(){
        if (fabs(_turningEase) < 0.05) {
            _turningEase = 0;
        }
        if (_turningEase > 0){
            _turningEase -= 0.05;
        }else if (_turningEase < 0){
                _turningEase += 0.05;
        }
    }
    
}

