#include "PlayState.h"

using namespace BaconBox;

namespace StreetMadness {
    PlayState::PlayState(const std::string &newName) : State(newName), _player(0), _paused(false), _currentScore(0) {
        
        InputManager::getDefaultKeyboard()->connectKeyHold(this, &PlayState::onKeyHold);
        InputManager::getDefaultKeyboard()->connectKeyRelease(this, &PlayState::onKeyRelease);
        InputManager::getDefaultPointer()->connectButtonRelease(this, &PlayState::onClick);
		
        ResourceManager::loadTextureRelativePath("player", "Char-Orange.png");
        ResourceManager::loadTextureRelativePath("rocketLaucher", "Launcher.png");
        ResourceManager::loadTextureRelativePath("carShield", "CarShield.png");
        ResourceManager::loadTextureRelativePath("rocket", "Rocket.png");
        ResourceManager::loadTextureRelativePath("exhaust", "exhaust.png");
        ResourceManager::loadTextureRelativePath("pause", "Pause.png");
        ResourceManager::loadTextureRelativePath("pauseScreen", "backgroundPause.png");
        ResourceManager::loadMusicRelativePath("gameMusic", "realDude.wav");
        ResourceManager::loadMusicRelativePath("musicShield", "shieldMusic.wav");
        
        ResourceManager::loadSoundRelativePath("rocketLauch", "rocketLaunch.wav");
        ResourceManager::loadSoundRelativePath("rocketLauch2", "rocketLaunch2.wav");
        
        camera.scaleFromPoint(2,2,Vector2(0,0));
        _player = new Player();
        _roadManager = new RoadManager();
        _hudManager = new HudManager();
        _pause = new Sprite("pause");
        _pauseScreen = new Sprite("pauseScreen");
        
	}
    PlayState::~PlayState(){
        delete _player;
        delete _roadManager;
        delete _hudManager;
        
    }
    void PlayState::onGetFocus(){
        MusicEngine::playBackgroundMusic("gameMusic");
        init();
    }
    void PlayState::init(){
        
        _currentScore = 0;
        _roadManager->init();
        Registry::playState = this;
        _player->init();
        _player->setPosition(this->getCamera().getWidth()/2 - _player->getWidth()/2,this->getCamera().getHeight()- _player->getHeight()*2);
        
        _hudManager->init();
        
        _pause->setAngle(90);
        _pause->setPosition(camera.getSize() - _pause->getSize());
        _pauseScreen->setAngle(90);
        _pauseScreen->setPosition(Vector2());
        
    }
    
    void PlayState::update() {
        if(_player->isDead()){
            Registry::score = _currentScore;
            Engine::playState("GameOverState");
        }
        if (!_paused) {
            if (_player->isShielded()) {
                _roadManager->update(MAX_CAR_SPEED);
                _currentScore += MAX_CAR_SPEED/10;
            }else{
                _roadManager->update(_player->getSpeed());
                _currentScore += _player->getSpeed()/10;
            }
            _roadManager->verifyCollision();
            
            _hudManager->update(_currentScore, _player->getLifeInPourcent(), _player->getRocketsInPourcent());
            _player->update();
            if (playerOnMargin() && !_player->isShielded()) {
                _player->applyHealthModifier(-0.25);
                camera.shake(0.005,0.2);
            }
            _hudManager->calculateLifeTransparency(_player);
        }else{
            _pauseScreen->update();
        }
        
        //player->update(InputManager::getDefaultAccelerometer()->getState().getXAcceleration());
    }
    
    void PlayState::render() {
        
        _roadManager->renderRoad();
        _roadManager->renderObstacles();
        _player->render();
        _roadManager->renderTrees();
        _hudManager->render();
        
        if (_paused) {
            _pauseScreen->render();
        }
        
        _pause->render();
        
    }
    
    void PlayState::onKeyHold(KeySignalData data){
        switch (data.key) {
            case Key::A:
                if (_player->getTurning() > -1) {
                    _player->turn(false);
                }
                break;
            case Key::D:
                if (_player->getTurning() < 1) {
                    _player->turn(true);
                }
                break;
            default:
                break;
        }
    }
    
    void PlayState::onKeyRelease(KeySignalData data){
        switch (data.key) {
            case Key::SPACE:
                _player->shootRocket();
                break;
            default:
                break;
        }
    }
    
    void PlayState::onClick(PointerButtonSignalData data){
        if (_paused) {
            _paused = false;
        }else if (_pause->getAxisAlignedBoundingBox().overlaps(camera.screenToWorld(data.getPosition()))) {
            _paused = true;
        }
    }
    
    const TileInfo* PlayState::getCurrentTileInfo(const BaconBox::Sprite* spriteOnTile) const{
        return _roadManager->getTileInfo(spriteOnTile->getPositionCenter().y);
    }
    bool PlayState::playerOnMargin(){
        //change the direction of the obstacle if outside borders
        const TileInfo* tile = getCurrentTileInfo(_player);
        
        float relativePosition = _player->getCentroid().x - Registry::playState->getCamera().getCentroid().x;
        if (tile) {
            
            if(tile->rightEnnemiLimit < relativePosition || tile->leftEnnemiLimit > relativePosition){
                return true;
            }
            //if the obstacle is between the medians go to the direction of the Relative position
            if (tile->medians.first < relativePosition && tile->medians.second > relativePosition) {
                return true;
            }
            return false;
        }
    }
}
