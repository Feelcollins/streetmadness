//
//  TileBag.cpp
//  BaconBoxApp
//
//  Created by Philippe Dubé-Tremblay on 12-05-13.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#include "TileBag.h"
#include "Registry.h"

using namespace BaconBox;

namespace StreetMadness {
    TileBag::TileBag(){
        
        load();
    }
    TileBag::~TileBag(){
    
        while (0 < bag.size()) {
            while (0 < bag[0].size()) {
                delete bag[0][bag[0].size()-1];
                bag[0].erase(--bag[0].end());
            }
            bag.erase(--bag.end());
        }
        while (0 < propBag.size()) {
            delete propBag[propBag.size()-1];
            propBag.erase(--propBag.end());
        }
    }
    RoadTile* TileBag::getRoadTile(std::list<RoadTile*> currentTiles){
        
        std::vector<int> childs = currentTiles.back()->getTileInfo().childs;
        int index = Random::getRandomInteger(0, childs.size()-1);
        
        return getSpecificRoadTile(currentTiles, childs[index]);
    }
    
    RoadTile* TileBag::getSpecificRoadTile(std::list<RoadTile*> currentTiles, int index){

        std::list<RoadTile*>::iterator it = currentTiles.begin();
        int copyNum = 0;
        while (it != currentTiles.end()) {
            if (copyNum > TILECOPY) {
                return 0;
            }
            if (bag[index][copyNum] == *it) {
                ++copyNum;
                it = currentTiles.begin();
            }else {
                it++;
            }
        }
        return bag[index][copyNum];
    }
    
    Sprite* TileBag::getProp(std::list<Sprite*> currentTrees)
    {
        std::list<Sprite*>::iterator it = currentTrees.begin();
        int index = 0;
        while (it != currentTrees.end()) {
            if(index >= TREE_BUFFER_SIZE){
                return 0;
            }
            if (propBag[index] == *it) {
                ++index;
                it = currentTrees.begin();
            }else {
                ++it;
            }
        }
        return propBag[index];
    }
    
    void TileBag::load(){
        
        std::string path = ResourcePathHandler::getResourcePathFor("tiles.json");
        
        JsonSerializer serializer;
        Value reader;
        serializer.readFromFile(path, reader);
        Object myObject = reader.getObject();
        Array tileSet = (myObject.find("tileset"))->second.getArray();
        for (int i=0; i<tileSet.size(); ++i) {
            
            BaconBox::Object myTile = tileSet[i].getObject();
            RoadTile* tempTile;
            
            TileInfo infos;
            
            infos.leftLimit = myTile.find("leftLimit")->second.getFloat();
            infos.rightLimit = myTile.find("rightLimit")->second.getFloat();
            infos.leftEnnemiLimit = myTile.find("leftEnnemiLimit")->second.getFloat();
            infos.rightEnnemiLimit = myTile.find("rightEnnemiLimit")->second.getFloat();
            infos.offset = myTile.find("offset")->second.getFloat();
            infos.rotationAngle = myTile.find("rotateAngle")->second.getFloat();
            
            //medians
            Array mediansArray = myTile.find("medians")->second.getArray();
            if (mediansArray.size()) {
                Object medians = mediansArray[0].getObject();
                infos.medians.first = medians.find("leftLimit")->second.getFloat();
                infos.medians.second = medians.find("rightLimit")->second.getFloat();
            }
            
            Array myArray = myTile.find("trees")->second.getArray();
            for (int j=0; j<myArray.size(); ++j) {
                Array myPos = myArray[j].getArray();
                infos.treesPositions.push_back(Vector2(myPos[0].getFloat(),myPos[1].getFloat()));
            }
            infos.treeRadius = myTile.find("treeRadius")->second.getFloat();
            
            //sets the possible childs
            myArray = myTile.find("child")->second.getArray();
            for (int j=0; j< myArray.size(); ++j) {
                infos.childs.push_back(myArray[j].getInt());
            }
            std::string path = myTile.find("image")->second.getString();

            ResourceManager::loadTextureRelativePath(path, path);
            
            std::vector<RoadTile*> tempVect;
            for (int j=0; j<=TILECOPY; ++j) {
                tempTile = new RoadTile(path, infos);
                tempVect.push_back(tempTile);
            }
            bag.push_back(tempVect);
        }
        //load trees
        ResourceManager::loadTextureRelativePath("tree1", "Arbre1.png");
        ResourceManager::loadTextureRelativePath("tree2", "Arbre2.png");
        ResourceManager::loadTextureRelativePath("tree3", "Arbre3.png");
        for (int i=0; i<TREE_BUFFER_SIZE; i+=3) {
            propBag.push_back(new Sprite("tree1"));
            propBag.push_back(new Sprite("tree2"));
            propBag.push_back(new Sprite("tree3"));
        }
    }
}