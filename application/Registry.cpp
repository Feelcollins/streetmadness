//
//  File.cpp
//  BaconBoxApp
//
//  Created by Philippe Dubé-Tremblay on 12-06-02.
//  Copyright (c) 2012 Anhero Inc. All rights reserved.
//

#include "Registry.h"

namespace StreetMadness {

    BaconBox::State* Registry::playState = NULL;
    std::string Registry::playerName = "";
    bool Registry::controlHorizontal = false;
    int Registry::score = 0;
    std::multimap<int, std::string, std::greater<int> > Registry::topTen;
}